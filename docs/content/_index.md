---
title: 'DAIMON Project (TU Clausthal)'
date: 2016-04-23T15:21:22+02:00
author: 'Abram Lawendy'
menu: main
type: homepage
weight: 10
draft: false
---

## Plugins - DAIMON Project (TU Clausthal)

### Table of contents

<!-- START doctoc generated TOC please keep comment here to allow auto update -->

<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->

-   [About the Project](#about-the-project)
-   [Documentation](#documentation)
-   [How to Install](#how-to-install)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## About the Project

Plugins for the [core](https://gitlab.com/tuc_graphql/core) backend for DAIMON Project (TU Clausthal).

## Documentation

For the **Documentation** and **How to Use** please visit one of the following websites:

-   core

    -   [hosted wiki with Hugo](https://tuc_graphql.gitlab.io/core)
    -   [project wiki on GitLab](https://gitlab.com/tuc_graphql/core/wikis/home)

-   plugins
    -   [hosted wiki with Hugo](https://tuc_graphql.gitlab.io/plugins)
    -   [project wiki on GitLab](https://gitlab.com/tuc_graphql/plugins/wikis/home)

## How to Install

### Docker Installation

-   Clone repository:

```console
git clone https://gitlab.com/tuc_graphql/plugins.git
```

-   Switch branch:

```sh
# Replace {BRANCH} with master (production) or staging (developement)
git checkout {BRANCH}
```

-   Login to [GitLab container registry](https://gitlab.com/help/user/project/container_registry):

```console
docker login registry.gitlab.com
```

-   Compose plugins with core:

```console
docker-compose up
```
