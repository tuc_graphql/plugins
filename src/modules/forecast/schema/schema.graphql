# Query coordination data
type coordType {
    # City geo location, latitude
    lat: Float
    # City geo location, longitude
    lon: Float
}
# Query city data
type cityType {
    id: ID
    # City name
    name: String
    coord: coordType
    # Country code (GB, JP etc.)
    country: String
}
type mainType {
    # Temperature
    temp: Float
    # Minimum temperature at the moment
    temp_min: Float
    # Maximum temperature at the moment
    temp_max: Float
    # Atmospheric pressure (on the sea level, if there is no sea_level or grnd_level data), hPa
    pressure: Float
    # Atmospheric pressure on the sea level, hPa
    sea_level: Float
    # Atmospheric pressure on the ground level, hPa
    grnd_level: Float
    # Humidity, %
    humidity: Int
    temp_kf: Float
}
# Query weather data
type weatherType {
    id: ID
    # Group of weather parameters (Rain, Snow, Extreme etc.)
    main: String
    # Weather condition within the group
    description: String
    icon: String
}
# Query wind data
type windType {
    # Wind speed. Unit Default: meter/sec, Metric: meter/sec, Imperial: miles/hour
    speed: Float
    # Wind direction, degrees (meteorological)
    deg: Float
}
# Query cloud data
type cloudsType {
    # Cloudiness, %
    all: Int
}
# Query system data
type sysType {
    pod: String
}
type dataType {
    dt: ID
    dt_txt: String
    main: mainType
    weather: [weatherType]
    clouds: cloudsType
    wind: windType
    sys: sysType
}
# Sample for current weather data
type forecastType {
    cod: String
    message: Float
    cnt: Int
    data: [dataType]
    city: cityType
    source: String
}

type Query {
    forecast: [forecastType]
    forecast_coord: [coordType]
    forecast_city: [cityType]
    forecast_main: [mainType]
    forecast_weather: [weatherType]
    forecast_clouds: [cloudsType]
    forecast_wind: [windType]
    forecast_sys: [sysType]
    forecast_data: [dataType]
}
