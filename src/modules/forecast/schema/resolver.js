// Import general parser for RESTFul endpoints
import { resolveDataFromREST } from 'class/parser/RESTParser.main';
// Import schema configuration from file and save it in variable CONFIG
import CONFIG from 'modules/forecast/config/config.json';

// Function to parse data from source URL
function forcastParser(CONFIG, schemaName) {
    const sources = CONFIG['source'];
    let promises = [];

    for (let i in sources) {
        // Save requested data (Promises) in array
        promises.push(
            resolveDataFromREST(
                sources[i].url.replace('{{ APPID }}', CONFIG.secret.TOKEN),
                sources[i]['mapping'][schemaName] || null,
                sources[i]['resolve'][schemaName] || null,
                sources[i]['dataLocation'][schemaName] || null
            )
        );
    }

    // Resolve promises and return the data
    return Promise.all(promises).then(responds => {
        let data = [];
        for (let i = 0; i < responds.length; i++) {
            // Push results to one array
            for (let j = 0; j < responds[i].length; j++) {
                data.push(responds[i][j]);
            }
        }
        return data;
    });
}

//  Define the queries resolvers with help of the general parser function for REST
exports.resolver = {
    Query: {
        forecast(root) {
            // Pass the schema configuration and the requested query
            return forcastParser(CONFIG, 'forecast');
        },
        forecast_coord(root) {
            return forcastParser(CONFIG, 'forecast_coord');
        },
        forecast_city(root) {
            return forcastParser(CONFIG, 'forecast_city');
        },
        forecast_main(root) {
            return forcastParser(CONFIG, 'forecast_main');
        },
        forecast_weather(root) {
            return forcastParser(CONFIG, 'forecast_weather');
        },
        forecast_clouds(root) {
            return forcastParser(CONFIG, 'forecast_clouds');
        },
        forecast_wind(root) {
            return forcastParser(CONFIG, 'forecast_wind');
        },
        forecast_sys(root) {
            return forcastParser(CONFIG, 'forecast_sys');
        },
        forecast_data(root) {
            return forcastParser(CONFIG, 'forecast_data');
        },
    },
};
